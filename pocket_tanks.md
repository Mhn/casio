
```
Prog "~SETUPVW"
File4
ClrText
" ":" ":" ":" ":" "
Locate 3,2,"* POCKET  TANKS *"
Locate 5,4,"1: NEW GAME"
Locate 5,5,"2: RESUME"
?→X
If X=1
Then Locate 5,5,"PLEASE WAIT..."
Locate 4,4,"GENERATING MAP,"
Seq(X,X,1,133,1→List 1
20→List 1[128
127-20→List 1[129
For 130→X To 133
100→List 1[X
Next
20Ran#+30→B
For 1→A To 127
4Ran#-2→X
B>50 And X>0 Or B<30 And X<0⇒-X→X
B+X→B
B→List 1[A
Next
1→U
127→V
Prog "~PT-DRWM"
Horizontal 63
Prog "~PT-UI"
0→θ
Else Cls
RclPict 2
IfEnd
Do
StoPict 2
For 128→A To 129
List 1[A→Z
Prog "~PT-DRWP"
Next
2*Frac (θ/2→r
List 1[128+r→Z
Text 1,r*120+2,"M"
Prog "~PT-MOVE"
Text 1,r*122+2,"S"
Prog "~PT-FIRE"
Isz θ
List 1[128→X
List 1[129→Y
LpWhile List 1[130]>0 And List 1[131]>0 And List 1[X]<63 And List 1[Y]<63
0→P
List 1[131]>0 And List 1[Y]<63⇒1→P
ClrText
Locate 5,2,"* GAME OVER *"
Locate 6,4,"PLAYER   IS"
Locate 13,4,P+1
Locate 6,5,"THE WINNER!"
Locate 2,7,"([EXE] FOR REMATCH)"
```



~SETUPVW

```
GridOff
AxesOff
CoordOff
LabelOff
ClrGraph
ViewWindow 1,127,0,63,1,0
StoV-Win 1
```



~PT-DRWM
In: U, V
Uses: W

```
U<1⇒1→U
V>127⇒127→V
Plot U,List 1[U
For U+1→W To V
Plot W,List 1[W
Line
Next
```



~PT-UI
Uses: I

```
For 1→I To 15
Text 2,29+2*I,"°"
Next
For 1→I To 2
Text 1,60+2*I,"I"
Next
For 1→I To 15
Text 2,65+2*I,"°"
Next
```



~PT-DRWP
In: Z

```
F-Line Z,List 1[Z],Z,List 1[Z]-9
```



~PT-MOVE
In: Z, I, r
Uses: X, Y, V, U, A
Out: Z

```
Plot Z,List 1[Z]-9    // wait for user input symbol
1→V
X<Z⇒-1→V
X-V→U
For Z→1 To U Step V
List 1[I+V]-List 1[I]<-3 Or List[I+V≥63⇒Break
PlotOn I,20
Next
I→List 1[128+r]
Cls
RclPict 2
For 128→A To 129
List 1[A→Z
Prog "~PT-DRWP"
Next
I→Z
```



~PT-FIRE

```
List 1[Int Z]-5→W
PlotZ,W    // wait for user input symbol
X-Int Z→X
Y-Int W→Y
0→R
X Or Y⇒(.5*sqrt(X²+Y²))^-1→R   // sqrt, ^-1 one symbol each
X*R→U
Y*R→V
Not R⇒1→R
While List 1[Int Z]>W
Z+U→Z
W+V→W
V+.4*R→V
PlotOn Z,W
Z<1 Or Z≥128⇒Break
WhileEnd
Int Z→Z
W→R
Cls
RclPict 2
If Z≥1 And Z≤127 And R<63
Then Prog "~PT-DMG"
Prog "~PT-BOOM"
Prog "~PT-UI2"
IfEnd
```



~PT-DMG

```
For 0→I To 1
List 1[128+I→U
List 1[U→V
sqrt((Z-U)²+(R-V)²→X   // sqrt one symbol
-11X+75→Y
List 1[130+I→List 1[132+I
If Y>0
Then List 1[130+I]-Y→List 1[130+I
IfEnd
Next
```



~PT-BOOM
```
5→r
Z-r→P
P<1⇒1→P
Z+r→Q
P→A
Q+1→B
Prog "~PT-DELM"
For P→U To Q
sqrt(r^2-(U-Z)^2)+R→X
X>63⇒63→X
List 1[U]<X⇒X→List 1[U
Next
P-1→U
Q+2→V
Prog "~PT-DRWM"
```



~PT-UI2
```
For 0→I To 1
2I-1→W
List 1[132+I→Y
Int (.31W*Y+58+9I→U
List 1[130+I→X
X<0⇒0→X
Int (.31W*X+58+9I→V
For U→Z To V Step -W
Text 1,Z," "
Next
Next
```



~PT-DELM
```
List 1[A→S~T
For A+1→W To B
List 1[W→X
X>S⇒X→S
X<T⇒X→T
Next
Int T-1→T
Int S+1→S
T<1⇒1→T
S>63⇒63→S
For T→V To S Step 5
For A→U To B Step 4
U→X
U+3>B⇒B-4→X
V→Y
V+4>S⇒S-5→Y
Text Y,X," "
Next
Next
```